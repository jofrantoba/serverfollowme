package com.followme.server.model.logic;

import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;

import com.followme.server.model.bean.Usuario;
import com.followme.server.model.dao.DaoUsuario;
import com.followme.server.model.dao.Querys;
import com.followme.shared.BeanParametro;
import com.followme.shared.UnknownException;

public class LogicUsuario {
	private PersistenceManager pm;

	public LogicUsuario(PersistenceManager pm) {
		this.pm = pm;
	}

	public boolean mantenimiento(BeanParametro parametro)
			throws UnknownException {
		DaoUsuario dao = new DaoUsuario(this.pm);
		return dao.mantenimiento(parametro);
	}


	public Usuario getBean(String id){
		try{
		DaoUsuario dao = new DaoUsuario(this.pm);
		return dao.getBean(id);
		}catch(Exception ex){
			return null;
		}
	}

	public Collection<Usuario> getListarBean() throws UnknownException {
		DaoUsuario dao = new DaoUsuario(this.pm);
		return dao.getListarBean();
	}
}

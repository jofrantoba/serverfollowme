package com.followme.server.model.dao;

import java.util.Collection;

import javax.jdo.PersistenceManager;

import com.followme.server.model.bean.Usuario;
import com.followme.server.model.dao.Querys;
import com.followme.shared.BeanParametro;
import com.followme.shared.UnknownException;

public class DaoUsuario {
	private PersistenceManager pm;

	public DaoUsuario(PersistenceManager pm) {
		this.pm = pm;
	}

	public boolean mantenimiento(BeanParametro parametro)
			throws UnknownException {
		Querys query = new Querys(this.pm);
		return query.mantenimiento(parametro);
	}
	
	public Usuario getBean(String id) throws UnknownException {
		Querys query = new Querys(this.pm);
		return (Usuario)query.getBean(Usuario.class, id);
	}

	@SuppressWarnings("unchecked")
	public Collection<Usuario> getListarBean() throws UnknownException {
		Querys query = new Querys(this.pm);
		Collection<Usuario> lista = (Collection<Usuario>) query
				.getListaBean(Usuario.class);
		return lista;
	}	
}

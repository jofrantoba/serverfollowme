package com.followme.server.model.bean;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;


@PersistenceCapable(detachable = "true")
public class PermisoUbicacion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PrimaryKey
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String idPermisoUbicacion;
	@Persistent
	private Date fechaInicio;
	@Persistent
	private Long tiempoInicio;
	@Persistent
	private Date fechaFin;
	@Persistent
	private Long tiempoFin;
	@Persistent
	private String estado;
	@Persistent
	private Usuario beanUsuario;
	@Persistent
	private String codeUsuario;
	@Persistent
	private Usuario beanPersonaConfianza;
	@Persistent
	private String codeUsuarioConfianza;
	@Persistent
	private Long version;
	@Persistent
	private String operacion;
	public String getIdPermisoUbicacion() {
		return idPermisoUbicacion;
	}
	public void setIdPermisoUbicacion(String idPermisoUbicacion) {
		this.idPermisoUbicacion = idPermisoUbicacion;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Long getTiempoInicio() {
		return tiempoInicio;
	}
	public void setTiempoInicio(Long tiempoInicio) {
		this.tiempoInicio = tiempoInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Long getTiempoFin() {
		return tiempoFin;
	}
	public void setTiempoFin(Long tiempoFin) {
		this.tiempoFin = tiempoFin;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Usuario getBeanUsuario() {
		return beanUsuario;
	}
	public void setBeanUsuario(Usuario beanUsuario) {
		this.beanUsuario = beanUsuario;
	}
	public String getCodeUsuario() {
		return codeUsuario;
	}
	public void setCodeUsuario(String codeUsuario) {
		this.codeUsuario = codeUsuario;
	}
	public Usuario getBeanPersonaConfianza() {
		return beanPersonaConfianza;
	}
	public void setBeanPersonaConfianza(Usuario beanPersonaConfianza) {
		this.beanPersonaConfianza = beanPersonaConfianza;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public String getCodeUsuarioConfianza() {
		return codeUsuarioConfianza;
	}
	public void setCodeUsuarioConfianza(String codeUsuarioConfianza) {
		this.codeUsuarioConfianza = codeUsuarioConfianza;
	}
	
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPermisoUbicacion == null) ? 0 : idPermisoUbicacion.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermisoUbicacion other = (PermisoUbicacion) obj;
		if (idPermisoUbicacion == null) {
			if (other.idPermisoUbicacion != null)
				return false;
		} else if (!idPermisoUbicacion.equals(other.idPermisoUbicacion))
			return false;
		return true;
	}
	
	
	
}

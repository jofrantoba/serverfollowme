package com.followme.server.model.bean;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(detachable = "true")
public class PersonaConfianza implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PrimaryKey
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String idPersonaConfianza;
	@Persistent
	private String estado;
	@Persistent
	private Long version;
	@Persistent
	private Usuario beanUsuario;
	@Persistent
	private String codeUsuario;
	@Persistent
	@Unowned
	private Usuario beanPersonaConfianza;
	@Persistent
	private String codeUsuarioConfianza;
	@Persistent
	private String operacion;
	
	public String getIdPersonaConfianza() {
		return idPersonaConfianza;
	}
	public void setIdPersonaConfianza(String idPersonaConfianza) {
		this.idPersonaConfianza = idPersonaConfianza;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public Usuario getBeanUsuario() {
		return beanUsuario;
	}
	public void setBeanUsuario(Usuario beanUsuario) {
		this.beanUsuario = beanUsuario;
	}
	public Usuario getBeanPersonaConfianza() {
		return beanPersonaConfianza;
	}
	public void setBeanPersonaConfianza(Usuario beanPersonaConfianza) {
		this.beanPersonaConfianza = beanPersonaConfianza;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public String getCodeUsuario() {
		return codeUsuario;
	}
	public void setCodeUsuario(String codeUsuario) {
		this.codeUsuario = codeUsuario;
	}
	public String getCodeUsuarioConfianza() {
		return codeUsuarioConfianza;
	}
	public void setCodeUsuarioConfianza(String codeUsuarioConfianza) {
		this.codeUsuarioConfianza = codeUsuarioConfianza;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPersonaConfianza == null) ? 0 : idPersonaConfianza.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaConfianza other = (PersonaConfianza) obj;
		if (idPersonaConfianza == null) {
			if (other.idPersonaConfianza != null)
				return false;
		} else if (!idPersonaConfianza.equals(other.idPersonaConfianza))
			return false;
		return true;
	}
	
	

}

package com.followme.server.model.bean;

import java.io.Serializable;
import java.util.Set;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable = "true")
public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9041156866931247879L;
	
	@PrimaryKey
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String correo;
	@Persistent
	private String nombre;
	@Persistent
	private String apellido;
	@Persistent
	private Long version;
	@Persistent
	private String celular;
	@Persistent
	private String tokeFirebase;
	@NotPersistent
	private String operacion;
	@Persistent(mappedBy = "beanUsuario") 
	private Set<PersonaConfianza> listConfianza;
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getTokeFirebase() {
		return tokeFirebase;
	}
	public void setTokeFirebase(String tokeFirebase) {
		this.tokeFirebase = tokeFirebase;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public Set<PersonaConfianza> getListConfianza() {
		return listConfianza;
	}
	public void setListConfianza(Set<PersonaConfianza> listConfianza) {
		this.listConfianza = listConfianza;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((correo == null) ? 0 : correo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (correo == null) {
			if (other.correo != null)
				return false;
		} else if (!correo.equals(other.correo))
			return false;
		return true;
	}

	
	
	
}

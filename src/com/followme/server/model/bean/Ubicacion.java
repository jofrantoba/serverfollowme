package com.followme.server.model.bean;

import java.io.Serializable;
import java.util.Date;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable = "true")
public class Ubicacion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 626931146620649648L;
	
	@PrimaryKey
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String idUbicacion;
	@Persistent
	private Double latitud;
	@Persistent
	private Double longitud;
	@Persistent
	private String pais;
	@Persistent
	private String region;
	@Persistent
	private String localidad;
	@Persistent
	private Date fechaUsuario;
	@Persistent
	private Long tiempoFechaUsuario;
	@Persistent
	private String timeZoneUsuario;
	@Persistent
	private Long version;
	@Persistent
	private String timeZoneServer;
	@Persistent
	private String geoService;
	@Persistent
	private Usuario beanUsuario;
	@Persistent
	private String codeUsuario;
	@NotPersistent
	private String operacion;
	
	
	public String getIdUbicacion() {
		return idUbicacion;
	}
	public void setIdUbicacion(String idUbicacion) {
		this.idUbicacion = idUbicacion;
	}
	public Double getLatitud() {
		return latitud;
	}
	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}
	public Double getLongitud() {
		return longitud;
	}
	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public String getGeoService() {
		return geoService;
	}
	public void setGeoService(String geoService) {
		this.geoService = geoService;
	}
	
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public Date getFechaUsuario() {
		return fechaUsuario;
	}
	public void setFechaUsuario(Date fechaUsuario) {
		this.fechaUsuario = fechaUsuario;
	}
	public Long getTiempoFechaUsuario() {
		return tiempoFechaUsuario;
	}
	public void setTiempoFechaUsuario(Long tiempoFechaUsuario) {
		this.tiempoFechaUsuario = tiempoFechaUsuario;
	}
	public String getTimeZoneUsuario() {
		return timeZoneUsuario;
	}
	public void setTimeZoneUsuario(String timeZoneUsuario) {
		this.timeZoneUsuario = timeZoneUsuario;
	}
	public String getTimeZoneServer() {
		return timeZoneServer;
	}
	public void setTimeZoneServer(String timeZoneServer) {
		this.timeZoneServer = timeZoneServer;
	}
	public Usuario getBeanUsuario() {
		return beanUsuario;
	}
	public void setBeanUsuario(Usuario beanUsuario) {
		this.beanUsuario = beanUsuario;
	}
	public String getCodeUsuario() {
		return codeUsuario;
	}
	public void setCodeUsuario(String codeUsuario) {
		this.codeUsuario = codeUsuario;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUbicacion == null) ? 0 : idUbicacion.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ubicacion other = (Ubicacion) obj;
		if (idUbicacion == null) {
			if (other.idUbicacion != null)
				return false;
		} else if (!idUbicacion.equals(other.idUbicacion))
			return false;
		return true;
	}

	
	

}

package com.followme.server.model.bean;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(detachable = "true")
public class CompartirUbicacion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PrimaryKey
	@Persistent
	@Extension(vendorName="datanucleus", key="gae.encoded-pk", value="true")
	private String idCompartirUbicacion;
	@Persistent
	@Unowned
	private Ubicacion beanUbicacion;
	@Persistent
	private String codeUbicacion;
	@Persistent
	@Unowned
	private PermisoUbicacion beanPermisoUbicacion;
	@Persistent
	private String codePermisoUbicacion;
	@Persistent
	private Long version;
	@NotPersistent
	private String operacion;
	public String getIdCompartirUbicacion() {
		return idCompartirUbicacion;
	}
	public void setIdCompartirUbicacion(String idCompartirUbicacion) {
		this.idCompartirUbicacion = idCompartirUbicacion;
	}
	public Ubicacion getBeanUbicacion() {
		return beanUbicacion;
	}
	public void setBeanUbicacion(Ubicacion beanUbicacion) {
		this.beanUbicacion = beanUbicacion;
	}
	public String getCodeUbicacion() {
		return codeUbicacion;
	}
	public void setCodeUbicacion(String codeUbicacion) {
		this.codeUbicacion = codeUbicacion;
	}
	public PermisoUbicacion getBeanPermisoUbicacion() {
		return beanPermisoUbicacion;
	}
	public void setBeanPermisoUbicacion(PermisoUbicacion beanPermisoUbicacion) {
		this.beanPermisoUbicacion = beanPermisoUbicacion;
	}
	public String getCodePermisoUbicacion() {
		return codePermisoUbicacion;
	}
	public void setCodePermisoUbicacion(String codePermisoUbicacion) {
		this.codePermisoUbicacion = codePermisoUbicacion;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	
}

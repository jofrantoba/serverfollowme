package com.followme.server.model.process;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import com.followme.server.model.bean.Usuario;
import com.followme.server.model.dao.PMF;
import com.followme.server.model.logic.LogicUsuario;
import com.followme.shared.BeanParametro;
import com.followme.shared.UnknownException;

public class GestionUsuario {
	
	public static boolean autenticacionOauth(Usuario beanUsuario) throws UnknownException{
		PersistenceManager pm=null;
		Transaction tx=null;
		try {
		pm=PMF.getPMF().getPersistenceManager();
		tx=pm.currentTransaction();
		tx.begin();
		LogicUsuario logic=new LogicUsuario(pm);
		BeanParametro parametro=new BeanParametro();
		parametro.setBean(beanUsuario);
		parametro.setTipoOperacion(BeanParametro.INSERTAR);		
		return logic.mantenimiento(parametro);
		} catch (UnknownException e) {
			// TODO Auto-generated catch block
			throw e;
		}finally{
			GestionShared.closeConnection(pm,tx);
		}
	}
	
}
